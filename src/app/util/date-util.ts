/**
 * 按给定格式格式化日期，默认是 yyyy-MM-dd hh:mm:ss
 * @param date 传入的日期
 * @param fmt 目标格式化样式 可选
 */
export function getFormatDate(date: Date, fmt = 'yyyy-MM-dd hh:mm:ss') {
    if (fmt == null || fmt == undefined) {
        fmt = 'yyyy-MM-dd hh:mm:ss';
    }
    var option = {
        "M+": date.getMonth() + 1,                      // 返回 Date 对象的月份 (0 ~ 11)
        "d+": date.getDate(),                           // 返回 Date 对象一个月中的某一天 (1 ~ 31)
        "h+": date.getHours(),                          // 返回 Date 对象的小时 (0 ~ 23)
        "m+": date.getMinutes(),                        // 返回 Date 对象的分钟 (0 ~ 59)
        "s+": date.getSeconds(),                        // 返回 Date 对象的秒数 (0 ~ 59)
        "S": date.getMilliseconds(),                    // 返回 Date 对象的毫秒 (0 ~ 999)
        "q+": Math.floor((date.getMonth() + 3) / 3)     // 季度 
    };
    if (/(y+)/.test(fmt)) {
        // RegExp.$1是RegExp的一个属性,指的是与正则表达式匹配的第一个 子匹配(以括号为标志)字符串
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    // for in 遍历对象的 key 
    for (var k in option) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (option[k]) : (("00" + option[k]).substr(("" + option[k]).length)));
        }
    }
    return fmt;
}


