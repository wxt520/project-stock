import { Component } from '@angular/core';
import { environment } from '../environments/environment';
// import { environment } from '../environments/environment.prod';
// import { environment } from '../environments/environment.test';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor() {
    console.log('微信号是：', environment.weixinhao);
  }
}
