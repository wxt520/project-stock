import { Component, OnInit } from '@angular/core';
import { Product, ProductService } from '../share/product.service';
import { FormControl } from '@angular/forms';
// tslint:disable
import "rxjs/Rx";
import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/observable/of';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  private imgURL = 'http://via.placeholder.com/320x150';
  private products: Observable<Array<Product>>;
  private keyWord: string;
  private titleFilter: FormControl = new FormControl();

  constructor(
    private productService: ProductService
  ) {
    // 监控title的值，结果中通过title管道过滤，后边会删除这个方法，用异步取代
    this.titleFilter.valueChanges
      .debounceTime(1000)
      .subscribe(
        value => this.keyWord = value
      );
  }

  ngOnInit() {
    this.products = this.productService.getProducts();
    this.productService.searchEvent.subscribe(
      params => this.products = this.productService.search(params),
      error => console.log(error),
      complete => console.log('数据流结束了...')
    );
  }

}

