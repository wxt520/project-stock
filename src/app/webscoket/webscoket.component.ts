import { Component, OnInit } from '@angular/core';
import { WebscoketService } from '../share/webscoket.service';

@Component({
  selector: 'app-webscoket',
  templateUrl: './webscoket.component.html',
  styleUrls: ['./webscoket.component.css']
})
export class WebscoketComponent implements OnInit {

  constructor(private wsService: WebscoketService) { }

  ngOnInit() {
    this.wsService.createObservableSocket("ws://localhost:8086").subscribe(
      data => console.log(data),
      err => console.log(err),
      () => console.log("流已经结束。")
    );
  }

  sendMessageToServer() {
    this.wsService.sendMessage("Hello from client");
  }

}
