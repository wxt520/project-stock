import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebscoketComponent } from './webscoket.component';

describe('WebscoketComponent', () => {
  let component: WebscoketComponent;
  let fixture: ComponentFixture<WebscoketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebscoketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebscoketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
