import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  /**
   * 对集合按指定的字段进行过滤
   * @param list 需要过滤的对象集合
   * @param filterField 过滤使用的字段
   * @param keyWord 用户输入的过滤值
   */
  transform(list: any[], filterField: string, keyWord: string): any {
    // console.log('list: ', list);
    // console.log('filterField: ', filterField);
    // console.log('keyWord: ', keyWord);
    if (!filterField || !keyWord) {
      console.log('检索过滤未执行...');
      return list;
    }
    return list.filter(item => {
      let val = item[filterField];
      return val.indexOf(keyWord) >= 0;
    });
  }

}
