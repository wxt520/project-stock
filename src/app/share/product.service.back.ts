import { Injectable } from '@angular/core';

@Injectable()
export class ProductService {

  private categories = ['电子产品', '硬件设施', '计算机', '编程', '物理', '实验研究', '书籍', '教育'];

  private products: Product[] = [
    new Product(1, '第一个商品', 1.99, 3.5, '这是一个商品，快来购买吧，逐步形成了一种独特的大学文化', [this.categories[0], this.categories[2], this.categories[1]]),
    new Product(2, '第二个商品', 2.99, 2.5, '这是二个商品，快来购买吧，逐步形成了一种独特的大学文化', [this.categories[0], this.categories[7]]),
    new Product(3, '第三个商品', 3.99, 4.5, '这是三个商品，快来购买吧，逐步形成了一种独特的大学文化', [this.categories[5], this.categories[4]]),
    new Product(4, '第四个商品', 4.99, 2, '这是四个商品，快来购买吧，逐步形成了一种独特的大学文化', [this.categories[3], this.categories[4]]),
    new Product(5, '第五个商品', 5.99, 5, '这是五个商品，快来购买吧，逐步形成了一种独特的大学文化', [this.categories[6]]),
    new Product(6, '第六个商品', 6.99, 3, '这是六个商品，快来购买吧，逐步形成了一种独特的大学文化', [this.categories[2], this.categories[3]]),
  ];

  private comments: Comment[] = [
    new Comment(1, 1, '2018-02-06 10:23:54', '张山', 4, '这个商品很好'),
    new Comment(2, 4, '2018-02-06 10:23:54', '张山', 3, '不不不不ububuu'),
    new Comment(3, 1, '2018-02-06 10:23:54', '李四', 3.5, '文档对象模型（Document Object Model，简称DOM），是W3C组织推荐的处理可扩展标志语言的标准编程接口。在网页上...'),
    new Comment(4, 5, '2018-02-06 10:23:54', '张山', 5, '这个商品很好'),
    new Comment(5, 2, '2018-02-06 10:23:54', '李四', 3.5, '这个商品很好'),
    new Comment(6, 4, '2018-02-06 10:23:54', '指挥所', 2, '这个商品很好'),
    new Comment(7, 1, '2018-02-06 10:23:54', '李四', 3.5, '不错的视频，大力支持喔，好好学习 天天向上'),
    new Comment(8, 3, '2018-02-06 10:23:54', '大坏蛋', 2, '这个商品很好'),
    new Comment(9, 3, '2018-02-06 10:23:54', '张山', 2, '这个商品很好'),
    new Comment(10, 2, '2018-02-06 10:23:54', '爱圆圆', 3, '这个女孩最美丽')
  ];

  constructor() { }

  /**
   * 查找所有商品
   */
  getProducts(): Product[] {
    return this.products;
  }

  /**
   * 根据id查找商品
   * @param proId
   */
  getProduct(proId: number): Product {
    // tslint:disable-next-line:triple-equals
    return this.products.find((product) => product.id == proId);
  }

  /**
   * 根据商品id查询评论staut
   * @param proId
   */
  getCommentByProductId(proId: number): Comment[] {
    return this.comments.filter(commment => commment.productId == proId);
  }

  /**
   * 获取商品的所有分类
   */
  getAllCategorys(): Array<string> {
    return this.categories;
  }

}

/**
 * 商品 Bean
 */
export class Product {
  constructor(
    public id: number,
    public title: string,
    public price: number,
    public rating: number, // 评分
    public desc: string,
    public categories: Array<string> // 商品类别
  ) { }
}

/**
 * 评论 Bean
 */
export class Comment {
  constructor(
    public id: number,
    public productId: number,
    public timestamp: string,
    public user: string,
    public rating: number,
    public content: string
  ) { }
}