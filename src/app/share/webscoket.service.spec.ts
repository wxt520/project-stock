import { TestBed, inject } from '@angular/core/testing';

import { WebscoketService } from './webscoket.service';

describe('WebscoketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebscoketService]
    });
  });

  it('should be created', inject([WebscoketService], (service: WebscoketService) => {
    expect(service).toBeTruthy();
  }));
});
