import { Injectable, EventEmitter } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// tslint:disable
import 'rxjs/Rx';

@Injectable()
export class ProductService {

  private categories = ['电子产品', '硬件设施', '计算机', '编程', '物理', '实验研究', '书籍', '教育'];

  private comments: Comment[] = [
    new Comment(1, 1, '2018-02-06 10:23:54', '张山', 4, '这个商品很好'),
    new Comment(2, 4, '2018-02-06 10:23:54', '张山', 3, '不不不不ububuu'),
    new Comment(3, 1, '2018-02-06 10:23:54', '李四', 3.5, '文档对象模型（Document Object Model，简称DOM），是W3C组织推荐的处理可扩展标志语言的标准编程接口。在网页上...'),
    new Comment(4, 5, '2018-02-06 10:23:54', '张山', 5, '这个商品很好'),
    new Comment(5, 2, '2018-02-06 10:23:54', '李四', 3.5, '这个商品很好'),
    new Comment(6, 4, '2018-02-06 10:23:54', '指挥所', 2, '这个商品很好'),
    new Comment(7, 1, '2018-02-06 10:23:54', '李四', 3.5, '不错的视频，大力支持喔，好好学习 天天向上'),
    new Comment(8, 3, '2018-02-06 10:23:54', '大坏蛋', 2, '这个商品很好'),
    new Comment(9, 3, '2018-02-06 10:23:54', '张山', 2, '这个商品很好'),
    new Comment(10, 2, '2018-02-06 10:23:54', '爱圆圆', 3, '这个女孩最美丽')
  ];

  searchEvent: EventEmitter<ProductSearchParams> = new EventEmitter<ProductSearchParams>();

  constructor(
    private http: Http
  ) { }

  /**
   * 查找所有商品
   */
  getProducts(): Observable<Product[]> {
    return this.http.get('/api/products').map(res => res.json());
  }

  /**
   * 根据id查找商品
   * @param proId
   */
  getProduct(proId: number): Observable<Product> {
    // tslint:disable-next-line:triple-equals
    // return this.products.find((product) => product.id == proId);
    return this.http.get('/api/product/' + proId).map(res => res.json());
  }

  /**
   * 根据商品id查询评论
   * @param proId
   */
  getCommentByProductId(proId: number): Observable<Comment[]> {
    // return this.comments.filter(commment => commment.productId == proId);
    return this.http.get('/api/product/' + proId + '/comments').map(res => res.json());
  }

  /** 
   * 获取商品的所有分类
   */
  getAllCategorys(): Array<string> {
    return this.categories;
  }

  search(params: ProductSearchParams): Observable<Product[]> {
    console.log('service search() 执行了。。。');
    return this.http.get('/api/products', { search: this.encodeParams(params) }).map(res => res.json());
  }

  private encodeParams(params: ProductSearchParams) {
    return Object.keys(params)
      .filter(key => params[key]) // 过滤后是一个 key 的数组
      .reduce((sum: URLSearchParams, key: string) => {
        sum.append(key, params[key]); // 拼接参数
        return sum;
      }, new URLSearchParams());
  }

}

/**
 * 商品 Bean
 */
export class Product {
  constructor(
    public id: number,
    public title: string,
    public price: number,
    public rating: number, // 评分
    public desc: string,
    public categories: Array<string> // 商品类别
  ) { }
}

/**
 * 评论 Bean
 */
export class Comment {
  constructor(
    public id: number,
    public productId: number,
    public timestamp: string,
    public user: string,
    public rating: number,
    public content: string
  ) { }
}

/**
 * 定义检索条件的bean
 */
export class ProductSearchParams {
  constructor(
    public title: string,
    public price: number,
    public category: string
  ) { }
}